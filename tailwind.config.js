/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [`./components/**/*.{vue,js,ts}`, `./pages/**/*.vue`],
  theme: {
    extend: {
      colors: {
        primary: "#F8D171",
        secondary: "#024D94",
      },
    },
    container: {
      center: true,
    },
  },
  plugins: [],
};
